var version = '1.0';
if (location.search.includes('v=1.1')) {
    version = '1.1';
}

if (location.search.includes('v=1.2')) {
    version = '1.2';
}

function processRequest(request, success, error, final) {
    request.setRequestHeader('Content-Type', 'application/json');
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status == 200) {
                var data;
                try {
                    data = JSON.parse(request.responseText);
                } catch (e) {
                    data = request.responseText;
                }
                console.log('ответ сервера:', data);
                success(data);
                final();

            } else if (request.status == 500) {
                alert('Критическая ошибка сервера');
                final();
            } else if (request.status >= 400) {
                console.log('сервер вернул ошибку:', request.responseText);
                alert(request.responseText);
                if (broken) {
                    alert(request.responseText);
                }
                error();
                final();
            }
        }
    }
}

function clear(e) {
    while (e.firstChild) {
        e.removeChild(e.lastChild);
    }
}
